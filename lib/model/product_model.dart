import 'dart:ui';

import 'package:commerce_app/core/utils/extension.dart';
class ProductModel {
  String? name, image,  size, price, description ,details;
  Color? color;

  ProductModel({
    this.name,
    this.image,
    this.color,
    this.size,
    this.price,
    this.description,
    this.details,
  });

  ProductModel.fromJson(Map productModel) {
    name = productModel['name'];
    image = productModel['image'];
    color = HexColor.fromHex(productModel['color']);
    size = productModel['size'];
    price = productModel['price'];
    description = productModel['description'];
    details = productModel['details'];
  }

  toJson() {
    return {
      'name': name,
      'image': image,
      'color': color,
      'size': size,
      'price': price,
      'description': description,
      'details' : details,
    };
  }
}
