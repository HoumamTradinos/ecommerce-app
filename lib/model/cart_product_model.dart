class CartProductModel {
  String? name, image, price;
  int quantity = 1;
  CartProductModel({this.name, this.image, this.price});

  CartProductModel.fromJson(Map cartModel) {
    name = cartModel['name'];
    image = cartModel['image'];
    price = cartModel['price'];
    quantity = cartModel['quantity'];
  }

  toJson() {
    return {
      'name': name,
      'image': image,
      'price': price,
      'quantity': quantity,
    };
  }

  changeQuantity(int qty) {
    quantity += qty;
    if(quantity < 0) quantity =0;
  }
}
