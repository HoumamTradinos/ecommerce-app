import 'package:commerce_app/core/services/fire_store_user.dart';
import 'package:commerce_app/core/utils/locale_storage_data.dart';
import 'package:commerce_app/main.dart';
import 'package:commerce_app/model/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthController extends GetxController {
  final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  late String email, password;
  String? name;

  ValueNotifier<bool> get loading => _loading;
  final ValueNotifier<bool> _loading = ValueNotifier(false);

  void googleSignIn() async {
    final GoogleSignInAccount? signInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication signInAuthentication =
        await signInAccount!.authentication;
    final AuthCredential authCredential = GoogleAuthProvider.credential(
      idToken: signInAuthentication.idToken,
      accessToken: signInAuthentication.accessToken,
    );
    try {
      await _firebaseAuth
          .signInWithCredential(authCredential)
          .then((user) async {
        await saveUserToFireStore(user);
        Get.offAllNamed('/home');
      });
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }

  void signInWithEmailAndPassword() async {
    try {
      await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password)
          .then(
        (user) async {
          await FireStoreUser().getCurrentUser(user.user!.uid).then(
            (fireUser) {
              print('Hamdan');
              print('fire store user \n ${fireUser.data()}');
              LocalStorageData.setUserData(
                UserModel.fromJson(
                  fireUser.data() as Map,
                ),
              );
            },
          );
          Get.offAllNamed('/home');
        },
      );
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }

  void createAccountWithEmailAndPassword() async {
    try {
      await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((user) async {
        await saveUserToFireStore(user);
        await FireStoreUser().getCurrentUser(user.user!.uid).then(
          (fireUser) {
            print('Hamdan');
            print('fire store user \n ${fireUser.data()}');
            LocalStorageData.setUserData(
              UserModel.fromJson(
                fireUser.data() as Map,
              ),
            );
          },
        );

        Get.offAllNamed('/home');
      });
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }

  Future<void> saveUserToFireStore(UserCredential user) async {
    UserModel userModel = UserModel(
        userId: user.user?.uid,
        email: user.user?.email,
        name: name ?? user.user?.displayName,
        pic: '');
    await FireStoreUser().addUserToFireStore(userModel);
    await setUserToLocalStorage(userModel);
  }

  setUserToLocalStorage(UserModel userModel) async {
    await LocalStorageData.setUserData(userModel);
  }
}
