import 'package:commerce_app/model/cart_product_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../services/database/cart_data_base_service.dart';

class CartController extends GetxController {
  ValueNotifier<bool> get loading => _loading;
  final ValueNotifier<bool> _loading = ValueNotifier(false);

  double get subTotal {
    if (_subTotal < 0) {
      return 0;
    } else {
      return _subTotal;
    }
  }

  double _subTotal = 0.0;

  List<CartProductModel> get cartProducts => _cartProducts;
  List<CartProductModel> _cartProducts = [];

  CartController() {
    getCartProducts();
  }

  getCartProducts() async {
    _loading.value = true;
    _cartProducts = await CartDataBaseService().getCartProducts();
    getSubTotal();
    _loading.value = false;
    update();
  }

  getSubTotal() {
    _subTotal = 0;
    for (var cartProduct in _cartProducts) {
      _subTotal +=
          (double.parse((cartProduct.price ?? '0')) * (cartProduct.quantity));
    }
    update();
  }

  addToCart(CartProductModel cartModel) async {
    // ensure that product is not in the cart already
    for (var cartProduct in _cartProducts) {
      if (cartModel.name == cartProduct.name) return;
    }

    //insert the product in the db && add the product to the cart
    _cartProducts.add(cartModel);
    await CartDataBaseService().insert(cartModel);
    update();
  }

  //increase qty of a product in the cart
  incQty(int index) async {
    _cartProducts[index].changeQuantity(1);
    await CartDataBaseService().changeQuantity(_cartProducts[index]);
    _subTotal += double.parse(_cartProducts[index].price ?? '0');
    update();
  }

  //decrease qty of a product in the cart
  decQty(int index) async {
    _cartProducts[index].changeQuantity(-1);
    await CartDataBaseService().changeQuantity(_cartProducts[index]);
    _subTotal -= double.parse(_cartProducts[index].price ?? '0');
    update();
  }

  checkOut() async {
    _cartProducts.clear();
    _subTotal = 0;
    await CartDataBaseService().checkOut();
    update();
  }
}
