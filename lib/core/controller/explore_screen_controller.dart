import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:commerce_app/model/product_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../model/category_model.dart';
import '../services/explore_services.dart';

class ExploreScreenController extends GetxController {
  final ValueNotifier<bool> _loading = ValueNotifier(false);
  ValueNotifier<bool> get loading => _loading;

  final List<CategoryModel> _categories = [];
  List<CategoryModel> get categories => _categories;

  final List<ProductModel> _bestSellingProducts = [];
  List<ProductModel> get bestSellingProducts => _bestSellingProducts;

  ExploreScreenController() {
    getCategories();
    getBestSelling();
  }

  getCategories() async {
    _loading.value = true;
    await ExpolreService().getCategories().then(
      (categories) {
        for (var category in categories) {
          _categories.add(
              CategoryModel.fromJson(category.data() as Map<dynamic, dynamic>));
        }
      },
    );
    _loading.value = false;
    update();
  }

  getBestSelling() async {
    _loading.value = true;
    await ExpolreService().getBestSelling().then(
      (bestSelling) {
        for (var product in bestSelling) {
          _bestSellingProducts.add(
              ProductModel.fromJson(product.data() as Map<dynamic, dynamic>));
        }
      },
    );
    _loading.value = false;
    update();
  }
}
