import 'package:cloud_firestore/cloud_firestore.dart';

class ExpolreService{
  final CollectionReference _categoriesReference =
  FirebaseFirestore.instance.collection('Categories');

  final CollectionReference _bestSoldProductReference =
  FirebaseFirestore.instance.collection('Best Sold Products');

  Future<List<QueryDocumentSnapshot>> getCategories() async{
    var categories = await  _categoriesReference.get();
    return categories.docs;
  }

  Future<List<QueryDocumentSnapshot>> getBestSelling() async{
    var bestSoldProducts = await  _bestSoldProductReference.get();
    return bestSoldProducts.docs;
  }

}