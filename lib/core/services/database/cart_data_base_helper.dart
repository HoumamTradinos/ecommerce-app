const String cartTable = 'cart';
const String nameColumn = 'name';
const String imageColumn = 'image';
const String priceColumn = 'price';
const String quantityColumn = 'quantity';