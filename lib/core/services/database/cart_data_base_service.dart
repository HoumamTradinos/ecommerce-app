import 'package:commerce_app/core/services/database/cart_data_base_helper.dart';
import 'package:commerce_app/model/cart_product_model.dart';
import 'package:commerce_app/model/product_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class CartDataBaseService {
  // Singleton pattern
  CartDataBaseService._internal();
  static final CartDataBaseService _cartDataBase =
      CartDataBaseService._internal();
  factory CartDataBaseService() => _cartDataBase;

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    // Initialize the DB first time it is accessed
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, 'cart_database.db');

    return await openDatabase(path, onCreate: _onCreate, version: 1);
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.execute(''' 
   CREATE TABLE $cartTable (
   $nameColumn TEXT NOT NULL,
   $imageColumn TEXT NOT NULL,
   $priceColumn TEXT NOT NULL,
   $quantityColumn INTEGER NOT NULL)
   ''');
  }

  // Define a function that inserts products into the cart
  Future<void> insert(CartProductModel cart) async {
    // Get a reference to the database.
    final db = await _cartDataBase.database;

    // Insert cart
    await db.insert(
      cartTable,
      cart.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the breeds from the breeds table.
  Future<List<CartProductModel>> getCartProducts() async {
    // Get a reference to the database.
    final db = await _cartDataBase.database;

    // Query the table for all the products in the cart.
    final List<Map<String, dynamic>> maps = await db.query(cartTable);

    // Convert the List<Map<String, dynamic> into a List<CartModel>.
    return List.generate(
        maps.length, (index) => CartProductModel.fromJson(maps[index]));
  }

  checkOut() async {
    final db = await _cartDataBase.database;
    await db.delete(cartTable);
  }

  changeQuantity(CartProductModel productModel) async {
    final db = await _cartDataBase.database;
    await db.update(
      cartTable,
      productModel.toJson(),
      where: '$nameColumn = ?',
      whereArgs: [productModel.name],
    );
  }
}
