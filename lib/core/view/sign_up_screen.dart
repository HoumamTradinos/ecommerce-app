import 'package:commerce_app/global/custom_text_style.dart';
import 'package:commerce_app/global/widgets/custom_app_bar.dart';

import 'package:flutter/material.dart';
import '../controller/auth_controller.dart';
import 'package:get/get.dart';

import 'widgets/custom_flat_button.dart';
import 'widgets/custom_text.dart';
import 'widgets/custom_text_form_field.dart';

class SignUpScreen extends GetView<AuthController> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  SignUpScreen({super.key});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Scaffold(
      appBar: CustomAppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Get.offNamed('/login'),
          color: Theme.of(context).iconTheme.color,
        ),
      ),
      body: Padding(
        padding:
            EdgeInsets.fromLTRB(width * 0.05, height * 0.1, width * 0.05, 0),
        child: SingleChildScrollView(
          child: Form(
            key: _key,
            child: Column(
              children: [
                const CustomText(
                  text: 'SignUp, ',
                  style: CustomTextStyle.xxLargeBold,
                ),
                SizedBox(
                  height: height * 0.075,
                ),
                CustomTextFormField(
                  text: 'Name',
                  hint: 'Hamdan',
                  onSave: (value) {
                    controller.name = value!;
                  },
                  validator: (value) {
                    if (value == null) print('Error , Null Name not allowed');
                    return null;
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                CustomTextFormField(
                  text: 'Email',
                  hint: 'iamhamdan@gmail.com',
                  onSave: (value) {
                    controller.email = value!;
                  },
                  validator: (value) {
                    if (value == null) print('Error , Null Email not allowed');
                    return null;
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                CustomTextFormField(
                  text: 'Password',
                  hint: '************',
                  onSave: (value) {
                    controller.password = value!;
                  },
                  validator: (value) {
                    if (value == null)
                      print('Error , Null password not allowed');
                    return null;
                  },
                ),
                SizedBox(
                  height: height * 0.075,
                ),
                CustomFlatButton(
                  text: 'Sign Up',
                  onPressed: () {
                    _key.currentState?.save();
                    if (_key.currentState!.validate()) {
                      controller.createAccountWithEmailAndPassword();
                    }
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
