import 'package:flutter/material.dart';
import 'custom_text.dart';

class CustomTextFormField extends StatelessWidget {
  final String text;
  final String? hint;
  final Function(String?)? onSave;
  final String? Function(String?)? validator;

   CustomTextFormField(
      {super.key, required this.text, this.hint, this.onSave, this.validator});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomText(
          text: text,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
        TextFormField(
          decoration: InputDecoration(
              hintText: hint,
              hintStyle: TextStyle(color: Colors.grey.shade500),
              fillColor: Colors.white,
              ),
          onSaved: onSave,
          validator: validator,
        ),
      ],
    );
  }
}
