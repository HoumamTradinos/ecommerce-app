import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../global/custom_text_style.dart';
import 'custom_text.dart';

class CustomProfileButton extends StatelessWidget {
  final String iconPath;
  final String title;
  final void Function()? onTap;

  const CustomProfileButton(
      {super.key, required this.iconPath, required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ,
      child: SizedBox(
        height: Get.height * .04,
        child: Row(

          children: [
            Image.asset(
              iconPath,
              fit: BoxFit.fill,
            ),
            SizedBox(
              width: Get.width * .05,
            ),
            Container(
              width: Get.width * .5,
              child: CustomText(
                text: title,
                style: CustomTextStyle.medium,
                alignment: Alignment.centerLeft,
              ),
            ),
            SizedBox(
              width: Get.width * .2,
            ),
            const Icon(
              Icons.arrow_forward_ios,
              size: 16,
            ),
          ],
        ),
      ),
    );
  }
}