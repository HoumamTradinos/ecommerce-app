import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'custom_text.dart';

class CustomFlatButton extends StatelessWidget {
  final String? text;
  final void Function()? onPressed;

  const CustomFlatButton({super.key, this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.all(Get.height * 0.02),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      onPressed: onPressed,
      color: Theme.of(context).primaryColor,
      child: CustomText(
        text: text ?? '',
        style: Theme.of(context).textTheme.bodyMedium,
        alignment: Alignment.center,
      ),
    );
  }
}
