import 'package:commerce_app/core/view/widgets/custom_flat_button.dart';
import 'package:commerce_app/core/view/widgets/custom_social_media_button.dart';
import 'package:commerce_app/core/view/widgets/custom_text.dart';
import 'package:commerce_app/core/view/widgets/custom_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../global/custom_text_style.dart';
import '../controller/auth_controller.dart';

class LoginScreen extends GetView<AuthController> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  LoginScreen({super.key});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return controller.loading.value
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : Scaffold(
            body: Padding(
              padding: EdgeInsets.fromLTRB(
                  width * 0.05, height * 0.2, width * 0.05, 0),
              child: SingleChildScrollView(
                child: Form(
                  key: _key,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const CustomText(
                            text: 'Welcome, ',
                            style: CustomTextStyle.xxLargeBold,
                          ),
                          GestureDetector(
                            onTap: () => Get.toNamed('/signup'),
                            child: const CustomText(
                              text: 'Sign Up',
                              style: CustomTextStyle.mediumPrimary,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.01,
                      ),
                      const CustomText(
                        text: 'Sign in to continue',
                        style: CustomTextStyle.captionText,
                      ),
                      SizedBox(
                        height: height * 0.03,
                      ),
                      CustomTextFormField(
                        text: 'Email',
                        hint: 'iamhamdan@gmail.com',
                        onSave: (value) {
                          controller.email = value!;
                        },
                        validator: (value) {
                          if (value == null)
                            print('Error , Null Email not allowed');
                          return null;
                        },
                      ),
                      SizedBox(
                        height: height * 0.05,
                      ),
                      CustomTextFormField(
                        text: 'Password',
                        hint: '************',
                        onSave: (value) {
                          controller.password = value!;
                        },
                        validator: (value) {
                          if (value == null)
                            print('Error , Null password not allowed');
                          return null;
                        },
                      ),
                      SizedBox(
                        height: height * 0.03,
                      ),
                      const CustomText(
                        text: 'forget password ?',
                        style: CustomTextStyle.xSmall,
                        alignment: Alignment.topRight,
                      ),
                      SizedBox(
                        height: height * 0.03,
                      ),
                      CustomFlatButton(
                        text: 'Sign In',
                        onPressed: () {
                          _key.currentState?.save();
                          if (_key.currentState!.validate()) {
                            controller.signInWithEmailAndPassword();
                          }
                        },
                      ),
                      SizedBox(
                        height: height * 0.05,
                      ),
                      CustomSocialMediaButton(
                        text: 'Sign In with Google',
                        svgPath: 'assets/images/google.svg',
                        onPressed: () {
                          controller.googleSignIn();
                        },
                      ),
                      SizedBox(
                        height: height * 0.05,
                      ),
                      CustomSocialMediaButton(
                        text: 'Sign In with Facebook',
                        svgPath: 'assets/images/facebook.svg',
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
