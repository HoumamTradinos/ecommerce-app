import 'package:cached_network_image/cached_network_image.dart';
import 'package:commerce_app/core/controller/cart_controller.dart';
import 'package:commerce_app/core/view/widgets/custom_flat_button.dart';
import 'package:commerce_app/core/view/widgets/custom_text.dart';
import 'package:commerce_app/global/custom_text_style.dart';
import 'package:commerce_app/model/cart_product_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controller/product_details_controller.dart';

class ProductDetailes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProductDetailsController productDetailsController = Get.find();
    productDetailsController.productModel = Get.arguments;
    return GetBuilder<ProductDetailsController>(
      init: productDetailsController,
      builder: (controller) => Scaffold(
        body: Column(
          children: [
            SizedBox(
              height: Get.height * .3,
              width: Get.width,
              child: CachedNetworkImage(
                fit: BoxFit.fill,
                progressIndicatorBuilder: (context, url, progress) => Center(
                  heightFactor: .2,
                  child: CircularProgressIndicator(
                    value: progress.progress,
                  ),
                ),
                imageUrl: controller.productModel?.image ?? '',
              ),
            ),
            SizedBox(
              height: Get.height * .02,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(
                Get.width * .05,
                0,
                Get.width * .05,
                0,
              ),
              child: Column(
                children: [
                  CustomText(
                    text: '${controller.productModel?.name}',
                    style: CustomTextStyle.xLargeBold,
                  ),
                  SizedBox(
                    height: Get.height * .02,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        width: Get.width * .4,
                        height: Get.height * .05,
                        padding: EdgeInsets.all(Get.width * .02),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const CustomText(
                              text: 'Size ',
                              style:CustomTextStyle.medium,
                            ),
                            CustomText(
                              text: '${controller.productModel?.size}',
                              style: CustomTextStyle.mediumBold,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: Get.width * .4,
                        height: Get.height * .05,
                        padding: EdgeInsets.all(Get.width * .02),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const CustomText(
                              text: 'Color ',
                              style: CustomTextStyle.medium,
                            ),
                            Container(
                              width: Get.width * .05,
                              height: Get.height * .025,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: controller.productModel?.color,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: Get.height * .015,
                  ),
                  const CustomText(
                    text: 'Details',
                    style: CustomTextStyle.largeBold,
                  ),
                  SizedBox(
                    height: Get.height * .02,
                  ),
                  SizedBox(
                    height: Get.height * .3,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: CustomText(
                        text: '${controller.productModel?.details}',
                        style: CustomTextStyle.highMedium,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(Get.width * .05,
                        Get.height * .12, Get.width * .05, 0),
                    height: Get.height * .2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            const CustomText(
                              text: 'PRICE',
                              style: CustomTextStyle.captionText,),
                            CustomText(
                              text: '\$${controller.productModel?.price}',
                              style: CustomTextStyle.largePrimaryBold),
                          ],
                        ),
                        GetBuilder<CartController>(
                          builder: (controller ) => Container(
                              margin: EdgeInsets.only(bottom: Get.height * .0175),
                              width: Get.width * .4,
                              child: CustomFlatButton(
                                text: 'ADD',
                                onPressed: () {
                                  controller.addToCart(
                                    CartProductModel(
                                      name: productDetailsController.productModel?.name,
                                      image: productDetailsController.productModel?.image,
                                      price: productDetailsController.productModel?.price,
                                    ),
                                  );
                                },
                              ),
                            ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
