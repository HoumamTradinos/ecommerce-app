import 'package:commerce_app/global/custom_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../controller/home_screen_controller.dart';
import 'explore_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    HomeScreenController homeScreenController = Get.find();
    return GetBuilder<HomeScreenController>(
      init: homeScreenController,
      builder: (controller) {
        return Scaffold(
          body: controller.currentScreen ?? const ExploreScreen(),
          bottomNavigationBar: bottomNavBar(),
        );
      },
    );
  }

  Widget bottomNavBar() {
    return GetBuilder<HomeScreenController>(
      builder: (controller) => BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            label: '',
            icon: Padding(
              padding: EdgeInsets.only(top: Get.height * 0.01),
              child: SvgPicture.asset('assets/svg/Icon_Explore.svg'),
            ),
            activeIcon: const Text(
              'Explore',
              style:  CustomTextStyle.xSmallBold,
            ),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Padding(
              padding: EdgeInsets.only(top: Get.height * 0.01),
              child: SvgPicture.asset('assets/svg/Icon_Cart.svg'),
            ),
            activeIcon: const Text(
              'Cart',
              style:  CustomTextStyle.xSmallBold,
            ),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Padding(
              padding: EdgeInsets.only(top: Get.height * 0.01),
              child: SvgPicture.asset('assets/svg/Icon_User.svg'),
            ),
            activeIcon: const Text(
              'Account',
              style: CustomTextStyle.xSmallBold,
            ),
          ),
        ],
        elevation: 0,
        currentIndex: controller.navBarValue,
        onTap: (index) => controller.changeSelectedValue(index),
      ),
    );
  }
}
