import 'dart:convert';
import 'package:commerce_app/model/user_model.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

const cachedUserData = 'CACHED_USER_DATA';

class LocalStorageData extends GetxController {

  Future<UserModel> get getUserModel => _getUserData();

  Future<UserModel> _getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return UserModel.fromJson(json.decode(prefs.getString(cachedUserData) ?? '',),);
  }

  static setUserData(UserModel user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(
      cachedUserData,
      json.encode(
        user.toJson(),
      ),
    );
    print('user saved to local storage \n ${user.toJson()}');
  }
  static deleteUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }
}
