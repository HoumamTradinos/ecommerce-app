import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
     this.title,
    this.backgroundColor,
    this.leading,
  }) : super(key: key);
  final String? title;
  final Color? backgroundColor;
  final Widget? leading;

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title ?? ''),
      backgroundColor: backgroundColor,
      leading: leading,
      elevation: 0,
    );
  }
}
