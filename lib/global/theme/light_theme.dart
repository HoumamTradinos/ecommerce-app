import 'package:flutter/material.dart';
import 'app_colors/app_light_colors.dart';

ThemeData getLightTheme() => ThemeData(
  progressIndicatorTheme: const ProgressIndicatorThemeData(
    color: AppLightColors.primaryColor
  ),
  scaffoldBackgroundColor: AppLightColors.scaffoldBackgroundColor,
      primaryColor: AppLightColors.primaryColor,
      appBarTheme: const AppBarTheme(
        color: AppLightColors.appBarColor,
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: AppLightColors.primaryColor),
      iconTheme:  const IconThemeData(color:AppLightColors.iconsColor, ) ,
      fontFamily: 'SourceSansPro',
    );
