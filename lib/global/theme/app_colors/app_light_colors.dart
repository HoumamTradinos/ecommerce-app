import 'package:flutter/material.dart';

class AppLightColors{
  static const Color primaryColor = Color.fromRGBO(0, 197, 105, 1);
  static const Color secondaryColor = Colors.black;
  static const Color appBarColor = Colors.white;
  static const Color iconsColor = Colors.black;
  static const Color scaffoldBackgroundColor = Colors.white;
  static  Color captionTextColor = Colors.grey.shade100;
}