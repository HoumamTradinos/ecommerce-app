class FontSize {
  static const double xxSmall = 12;
  static const double xSmall = 14;
  static const double small = 16;
  static const double medium = 18;
  static const double large = 20;
  static const double xLarge = 26;
  static const double xxLarge = 30;
}
