import 'package:commerce_app/global/font_size.dart';
import 'package:commerce_app/global/theme/app_colors/app_light_colors.dart';
import 'package:flutter/material.dart';

class CustomTextStyle {
  // all app text styles here

  //     #Caption Size
  static const TextStyle captionText =
      TextStyle(fontSize: FontSize.xSmall, color: Colors.grey);

  //     #Large Size
  static const TextStyle largeBold = TextStyle(
    fontSize: FontSize.large,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle largePrimaryBold = TextStyle(
      fontSize: FontSize.large,
      color: AppLightColors.primaryColor,
      fontWeight: FontWeight.bold);

  static const TextStyle xLargeBold = TextStyle(
    fontSize: FontSize.xLarge,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle xLarge = TextStyle(
    fontSize: FontSize.xLarge,
  );

  static const TextStyle xxLargeBold = TextStyle(
    fontSize: FontSize.xxLarge,
    fontWeight: FontWeight.bold,
  );

  //     #Medium Size

  static const TextStyle medium = TextStyle(fontSize: FontSize.medium);

  static const TextStyle mediumBold = TextStyle(
    fontSize: FontSize.medium,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle mediumPrimary =
      TextStyle(fontSize: FontSize.medium, color: AppLightColors.primaryColor);

  static const TextStyle highMedium =
      TextStyle(fontSize: FontSize.medium, height: 2);

  //     #Small Size

  static const TextStyle small = TextStyle(fontSize: FontSize.small);

  static const TextStyle xSmall = TextStyle(fontSize: FontSize.xSmall);

  static const TextStyle xSmallBold =
      TextStyle(fontSize: FontSize.xSmall, fontWeight: FontWeight.bold);

  static const TextStyle smallPrimary =
      TextStyle(fontSize: FontSize.small, color: AppLightColors.primaryColor);
}
